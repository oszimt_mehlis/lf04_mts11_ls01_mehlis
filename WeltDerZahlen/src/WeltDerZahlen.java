

	/**
	  *   Aufgabe:  Recherechieren Sie im Internet !
	  * 
	  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
	  *
	  *   Vergessen Sie nicht den richtigen Datentyp !!
	  *
	  *
	  * @version 1.0 from 21.08.2019
	  * @author << Ihr Name >>
	  */

	public class WeltDerZahlen {

	  public static void main(String[] args) {
	    
	    /*  *********************************************************
	    
	         Zuerst werden die Variablen mit den Werten festgelegt!
	    
	    *********************************************************** */
	    // Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem                    
	    int anzahlPlaneten =  8;
	    
	    // Anzahl der Sterne in unserer Milchstra�e
	    long anzahlSterne = 400000000000l;
	    
	    // Wie viele Einwohner hat Berlin?
	    int bewohnerBerlin = 3700000;
	    
	    // Wie alt bist du?  Wie viele Tage sind das?
	    
	    short alterTage = 6915;
	    
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
	    int gewichtKilogramm = 150000;  
	    
	    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
	    int flaecheGroessteLand = 17130000;
	    
	    // Wie gro� ist das kleinste Land der Erde?
	    
	    float flaecheKleinsteLand = 0.44f;
	    
	    
	    
	    
	    /*  *********************************************************
	    
	         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
	    
	    System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten);
	    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
	    System.out.println("Einwohner Berlins: " + bewohnerBerlin);
	    System.out.println("Mein Alter in Tagen betr�gt: " +  alterTage + " Tage.");
	    System.out.println("Der Blauwal hat ein Gewicht von: " + gewichtKilogramm + " kg.");
	    System.out.println("Die Fl�che von Russland betr�gt: " + flaecheGroessteLand + " km�");
	    System.out.println("Der Vatikanstadt hat eine F�che von: " + flaecheKleinsteLand + " km�");
	    System.out.println(" *******  Ende des Programms  ******* ");
	    
	  }
	}