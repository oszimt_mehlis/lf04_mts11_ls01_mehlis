
public class Aufgabe1 {

	public static void main(String[] args) {
		System.out.print("Heute ist ein sch�ner Tag. ");
		System.out.println("Denn bald ist Wochenende!\n");
		
		String s = "ist";
		System.out.print("Heute " + s + " ein \"sch�ner Tag\".\n");
		System.out.print("\nDenn bald " + s + " \'Wochenende\'!\n");
		
		// Der Unterschied zwischen den Anweisungen print und println besteht darin, dass nach der Anweisung println ein Zeilenumbruche gemacht wird.
		// Bei print hingegen werden alle folgenden Anweisungen direkt dahinter geschrieben.
	}

}
