import java.util.Scanner;

class FahrkartenautomatTicketgrenzen {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		Scanner tastatur = new Scanner(System.in);
		zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

		// Geldeinwurf/Fahrkartenbezhalung
		// -------------------------------
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// R�ckgeldberechnung und -Ausgabe
		// --------------------------------
		rueckgeldAusgeben(rueckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wuenschen Ihnen eine gute Fahrt.");

		tastatur.close();

	}

	public static double fahrkartenbestellungErfassen(Scanner t1) {
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zZB;
		zZB = t1.nextDouble();
		System.out.print("Anzahl der Tickets (zwischen 1 und 10): ");
		double anzahlTickets = t1.nextInt();
		if ((anzahlTickets >= 10) || (anzahlTickets <= 1)) {
			anzahlTickets = 1;
			System.out.println("Ihre Eingabe ist ung�ltig. Deshalb wurde Ihre Ticketzahl auf 1 ge�ndert!");
		}
		zZB = anzahlTickets * zZB;
		return zZB;
	}

	public static double fahrkartenBezahlen(double zuZahlen, Scanner t2) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMuenze = t2.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		double rueckgabe = eingezahlterGesamtbetrag - zuZahlen;
		return rueckgabe;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(250);
		System.out.println("\n");
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("===");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void rueckgeldAusgeben(double rueckgabe) {
		if (rueckgabe > 0.0) {
			System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f%s\n", rueckgabe, " EURO");
			System.out.println("wird in folgenden Muenzen ausgezahlt:");

			while (rueckgabe >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabe -= 2.0;
			}
			while (rueckgabe >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabe -= 1.0;
			}
			while (rueckgabe >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabe -= 0.5;
			}
			while (rueckgabe >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabe -= 0.2;
			}
			while (rueckgabe >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabe -= 0.1;
			}
			while (rueckgabe >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabe -= 0.05;
			}
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.printf("%d %s\n", betrag, einheit);

	}

}