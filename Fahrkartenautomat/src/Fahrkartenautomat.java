﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		double rueckgabebetrag;
		int anzahlTickets;

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		

		//Ticketangabe anzeigen
		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag*anzahlTickets) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag*anzahlTickets - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		// Rückgeldberechnung und -Ausgabe  
		// --------------------------------
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag*anzahlTickets;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s\n", rueckgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2,00 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1,00 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		tastatur.close();
	
		/*Der Datentyp für die Anzahl der Tickets ist ein Integer, da die Anzahl der Tickets nur eine gerade
		Zahl sein kann. Also wird kein Float oder Double benötigt. Bei Speicherproblemen kann auch anstatt
		des Integer Byte oder Short als Datentyp genutzt werden.
		
		Um nun den richtigen Preis zu ermitteln, der zu zahlen ist, muss man die Anzahl der Ticktes mit dem
		Einzelpreis, der zu zahlen ist multiplitieren. Bei der Multiplikation von ticketAnzahl mit zuZahlenderBetrag
		wird also der neu entstandene Preis für x Tickets berechnet.*/
	}
}