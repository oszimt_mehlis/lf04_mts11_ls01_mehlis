import java.util.Scanner;

class FahrkartenautomatMethoden {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;
		int anzahlTickets;

		Scanner tastatur = new Scanner(System.in);

		zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

		// Ticketangabe anzeigen
		// ---------------------
		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();

		// Geldeinwurf/Fahrkartenbezhalung
		// -------------------------------
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, anzahlTickets, tastatur);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// R�ckgeldberechnung und -Ausgabe
		// --------------------------------
		rueckgeldAusgeben(rueckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wuenschen Ihnen eine gute Fahrt.");
		tastatur.close();

	}

	public static double fahrkartenbestellungErfassen(Scanner t1) {
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zZB;
		zZB = t1.nextDouble();
		return zZB;
	}

	public static double fahrkartenBezahlen(double zuZahlen, double anzTickets, Scanner t2) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen * anzTickets) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen * anzTickets - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMuenze = t2.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		double rueckgabe = eingezahlterGesamtbetrag - zuZahlen * anzTickets;
		return rueckgabe;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(250);
		System.out.println("\n\n");
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("===");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void rueckgeldAusgeben(double rueckgabe) {
		if (rueckgabe > 0.0) {
			System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f%s\n", rueckgabe, " EURO");
			System.out.println("wird in folgenden Muenzen ausgezahlt:");
			
			while (rueckgabe >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabe -= 2.0;
			}
			while (rueckgabe >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabe -= 1.0;
			}
			while (rueckgabe >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabe -= 0.5;
			}
			while (rueckgabe >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabe -= 0.2;
			}
			while (rueckgabe >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabe -= 0.1;
			}
			while (rueckgabe >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabe -= 0.05;
			}
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
			System.out.printf("%d %s\n", betrag, einheit);
		
	}

}