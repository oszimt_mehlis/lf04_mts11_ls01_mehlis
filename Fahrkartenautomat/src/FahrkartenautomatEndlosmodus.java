import java.util.Scanner;

class FahrkartenautomatEndlosmodus {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;
		int vorgangWiederholen;
		boolean erneuterKauf = true;

		Scanner tastatur = new Scanner(System.in);
		while (erneuterKauf == true)
		{
			// Fahrkartenerfassung
			// -------------------
			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
	
			// Geldeinwurf/Fahrkartenbezahlung
			// -------------------------------
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
	
			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();
	
			// R�ckgeldberechnung und -Ausgabe
			// --------------------------------
			rueckgeldAusgeben(rueckgabebetrag);
	
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wuenschen Ihnen eine gute Fahrt.\n");
			
			warte(600);
			System.out.println("\n\n");
		}
		
		tastatur.close();

	}

	public static double fahrkartenbestellungErfassen(Scanner t1) {
		int fahrkartenWahl = 0;
		System.out.println("Fahrkartenbestellung:");
		System.out.println("======================\n");
		System.out.println("Bitte w�hlen Sie ihre gew�nschte Fahrkarte f�r Berlin AB aus (Nummer der Karte eingeben):");
		System.out.printf("%48s\n", "Einzefahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.printf("%43s\n", "Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.printf("%56s\n\n", "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		while ((fahrkartenWahl < 1) || (fahrkartenWahl > 3))
		{			
			System.out.print("Ihre Wahl: ");
			fahrkartenWahl = t1.nextInt();
			if ((fahrkartenWahl < 1) || (fahrkartenWahl > 3))
			{
				System.out.println("  >> ung�ltige Eingabe <<");
			}
		}
		
		double zZB = 0;
		if (fahrkartenWahl == 1)
		{
			zZB = 2.90;
		}
		else if (fahrkartenWahl == 2)
		{
			zZB = 8.60;
		}
		else if (fahrkartenWahl == 3)
		{
			zZB = 23.50;
		}
		
		System.out.print("Anzahl der Tickets (zwischen 1 und 10): ");
		double anzahlTickets = t1.nextInt();
		if ((anzahlTickets >= 10) || (anzahlTickets <= 1))
		{
			anzahlTickets = 1;
			System.out.println("Ihre Eingabe ist ung�ltig. Deshalb wurde Ihre Ticketzahl auf 1 ge�ndert!");
		}
		zZB = anzahlTickets * zZB;
		return zZB;		
	}

	public static double fahrkartenBezahlen(double zuZahlen, Scanner t2) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMuenze = t2.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		double rueckgabe = eingezahlterGesamtbetrag - zuZahlen;
		return rueckgabe;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(250);
		System.out.println("\n");
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("===");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void rueckgeldAusgeben(double rueckgabe) {
		if (rueckgabe > 0.0) {
			System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f%s\n", rueckgabe, " EURO");
			System.out.println("wird in folgenden Muenzen ausgezahlt:");
			
			while (rueckgabe >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabe -= 2.0;
			}
			while (rueckgabe >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabe -= 1.0;
			}
			while (rueckgabe >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabe -= 0.5;
			}
			while (rueckgabe >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabe -= 0.2;
			}
			while (rueckgabe >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabe -= 0.1;
			}
			while (rueckgabe >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabe -= 0.05;
			}
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
			System.out.printf("%d %s\n", betrag, einheit);
		
	}

}