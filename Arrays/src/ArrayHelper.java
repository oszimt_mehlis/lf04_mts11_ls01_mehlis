
public class ArrayHelper {

	public static void main(String[] args) {
		
		int [] zahlen = new int [] {0,1,2,3,4,5,6,7,8,9};
			
		System.out.println(convertArrayToString(zahlen));
				
	}
	
	public static String convertArrayToString(int [] zahlen) {
		String zahlenkette = "";
		for (int i = 0; i < zahlen.length; i++) {
			zahlenkette = zahlenkette + zahlen[i] + ',';
		}	
		return zahlenkette;
	}

}
