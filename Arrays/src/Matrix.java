
public class Matrix {

	public static void main(String[] args) {
	
		double matrix [][] = new double [][] {{4,3},{3,4}};
		System.out.println(matrixPruefen(matrix));

	}
	
	public static boolean matrixPruefen (double matrix [][]) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix [i][j] != matrix [j][i]) {
					return false;				
				}
			}
		}
		return true;		
	}

}
