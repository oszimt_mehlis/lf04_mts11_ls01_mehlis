
public class Temperaturtabelle {

	public static void main(String[] args) {

		System.out.println("Temperaturtabelle: ");
		System.out.printf("%-10s | %10s\n", "Fahrenheit", "Celsius");
		System.out.println("-------------------------");
		temperaturenAusgeben(5);
	}

	public static void temperaturenAusgeben(int anzahlWerte) {
		double temperaturen[][] = new double[anzahlWerte][2];

		for (int i = 0; i < anzahlWerte; i++) {
			temperaturen[i][0] = 0 + i * 10;
			temperaturen[i][1] = (5.0 / 9) * (temperaturen[i][0] - 32);
			System.out.printf("%-10.2f | %10.2f\n", temperaturen[i][0], temperaturen[i][1]);
		}

	}

}
