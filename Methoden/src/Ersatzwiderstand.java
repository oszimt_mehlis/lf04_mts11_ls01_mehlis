
public class Ersatzwiderstand {

	public static void main(String[] args) {
		double r1 = 100;
		double r2 = 500;
		ersatzwiderstandReihenschaltungBerechnen(r1, r2);
		ersatzwiderstandParallelschaltungBerechnen(r1, r2);
	}
	public static void ersatzwiderstandReihenschaltungBerechnen (double r1, double r2) {
		double ersatzwiderstandR = r1 + r2;
		System.out.printf("Der Ersatzwiderstand f�r die Reihenschaltung mit den Widerst�nden r1 = %.2f und "
				+ "r2 = %.2f betr�gt %.2f.", r1, r2, ersatzwiderstandR);
	}
	
	public static void ersatzwiderstandParallelschaltungBerechnen (double r1, double r2) {
		double ersatzwiderstandP = (r1 * r2) / (r1 + r2);
		System.out.printf("\nDer Ersatzwiderstand f�r die Parallelschaltung mit den Widerst�nden r1 = %.2f und "
				+ "r2 = %.2f betr�gt %.2f.", r1, r2, ersatzwiderstandP);
	}
}
