
public class Volumenberechnung {

	public static void main(String[] args) {
		double a = 5;
		double b = 3;
		double c = 4;
		double h = 2.5;
		double r = 5;
		double pi = Math.PI;
		volumenWürfelBerechnen(a);
		volumenQuaderBerechnen(a,b,c);
		volumenPyramideBerechnen(a, h);
		volumenKegelBerechnen(r, pi);		

	}
	
	public static void volumenWürfelBerechnen(double a) {
		double volumenWürfel = a*a*a;
		System.out.printf("%s%.2f", "Volumen für den Würfel: ", volumenWürfel, " (V = a * a * a)");		
	}
	
	public static void volumenQuaderBerechnen(double a, double b, double c) {
		double volumenQuader = a*b*c;
		System.out.printf("\n%s%.2f", "Volumen für den Quader: ", volumenQuader, " (V = a * b * c)");
	}
	
	public static void volumenPyramideBerechnen(double a, double h) {
		double volumenPyramide = (a*a*h)/3;
		System.out.printf("\n%s%.2f", "Volumen für die Pyramide: ", volumenPyramide, " (V = (a * a * h) / 3)");
	}
	
	public static void volumenKegelBerechnen(double r, double pi) {
		double volumenKugel = 4/3*r*r*r*pi;
		System.out.printf("\n%s%.2f", "Volumen für den Kegel: ", volumenKugel, " (V = 4/3 * r * r * r * pi)");
	}

}
