
public class Multiplikation {

	public static void main(String[] args) {
		
		double zahl1 = 2.36;
		double zahl2 = 7.87;
		double ergebnis = multiplizieren(zahl1, zahl2);
		System.out.println("Das Ergebnis lautet: " + ergebnis);
	}
		
	public static double multiplizieren (double z1, double z2) {
		double erg = z1 * z2;
		return erg;
	
	}
	

}
