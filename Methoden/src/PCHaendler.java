import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("Was m�chten Sie bestellen?");
		String artikel = liesString(myScanner.next());
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(myScanner.nextInt());

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner.nextDouble());

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner.nextDouble());

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		System.out.println("\tRechnung");
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
	
	public static String liesString(String text) {
		String artikel = text;
		return artikel;
	}
	
	public static int liesInt(int z1) {
		int anzahl = z1;
		return anzahl;
	}
	
	public static double liesDouble(double z2) {
		double eingegebeneZahl = z2;
		return eingegebeneZahl;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double netto = anzahl*nettopreis;
		return netto;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double brutto = nettogesamtpreis * (1 + mwst / 100);
		return brutto;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, 
			double bruttogesamtpreis, double mwst) {
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}