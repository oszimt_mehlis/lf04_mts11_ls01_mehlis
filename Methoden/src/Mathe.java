
public class Mathe {

	public static void main(String[] args) {
		double x = 5;
		double quadrat = quadrat(x);
		System.out.printf("x = %.2f und x^2 = %.2f", x, quadrat);
		
		double kathete1 = 3;
		double kathete2 = 4;
		double h = Mathe.hypothenuse(kathete1, kathete2);
		System.out.printf("\nDie L�nge der einen Kathete betr�gt %.2f und die L�nge der anderen Kathete betr�gt %.2f.\nDie L�nge "
				+ "der Hypthenuse betr�gt somit %.2f.", kathete1, kathete2, h);
	}
	
	public static double quadrat (double x) {
		double quadrat = x * x;
		return quadrat;
	}
	
	public static double hypothenuse (double k1, double k2) {
		double h = Math.sqrt(quadrat(k1)+ quadrat(k2));
		return h;
	}
}
