import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);
		double v = 0.00;
		System.out.print("Bitte geben sie ein, wie die Geschwindigkeit ver�ndert werden soll.\n"
				+ "Eine Zahl mit + bedeutet beschleunigen und mit einem - bedeutet bremsen.\n");
		while (v>=0 && v<=130) {
			System.out.print("Neue Beschleunigung: ");
			v = beschleunige(v, Eingabe.nextDouble());
			System.out.printf("\nDie aktuelle Geschwindigkeit betr�gt %.2f km/h.\n\n" , v);
		}
	}
	
	public static double beschleunige(double v, double dv) {
		if (v>=0 && v<=130) 
		{
			if (v==0 && dv<0) 
			{
				System.out.print("Die aktuelle Geschwindigkeit konnte nicht ge�ndert werden.");
				return v;
			}
			else if (v==130 && dv>0) 
			{
				System.out.print("Die aktuelle Geschwindigkeit konnte nicht ge�ndert werden.");
				return v;
			}
			
			if (v>=0 && v<=130) 
				v = v + dv;
			
			if (v<=0) 
			{
				v = 0;
				System.out.print("Das Fahrzeug wurde nun bis zur Geschwindigkeit 0 km/h gebremst und ist zum Stillstan gekommen.");	
			}
			else if (v >= 130) 
			{
				v = 130;
				System.out.print("Das Fahrzeug wurde nun bis zur maximalen Geschwindigkeit von 130 km/h beschleunigt.");
			}
			else if (v>=0 && v<=130 && dv<0)
				System.out.printf("Das Fahrzeug wurde nun bis zur Geschwindigkeit %.2f km/h gebremst.", v);
			else if (v>=0 && v<=130 && dv>0)
				System.out.printf("Das Fahrzeug wurde nun bis zur Geschwindigkeit %.2f km/h beschleunigt.", v);
		}
		else if (dv>0 && v>130)
			System.out.println("Die maximale Geschwindikeit von 130 km/h wurde bereits erreicht.");
		else if (dv<0 && v<0)
			System.out.println("Die minimale Geschwindikeit von 0 km/h wurde bereits erreicht.");
		
		return v;
	}

}
