import java.util.Scanner;

public class Aufgabe6_while { 
	//Aufgabe Million
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		double einlage = 0;
		double zinssatz = 0;
		double jaehrlicherGewinn = 0;
		int anzahlJahre = 1;
		
		System.out.println("Guten Tag, dies ist ein Programm, welches berechnet, nach wie vielen Jahren Sie durch Ihre Einlage Million�r werden.");
		System.out.println("\nBitte geben Sie ein, wie viel Geld Sie einlegen wollen:");
		einlage = eingabe.nextDouble();
		
		System.out.println("Bitte geben Sie noch den monatlichen Zinssatz ein:");
		zinssatz = eingabe.nextDouble();
		
		jaehrlicherGewinn = (einlage + ((einlage * zinssatz) /100));
		einlage = jaehrlicherGewinn;
		
		while (einlage <= 1000000)
		{
			einlage = (einlage + ((einlage * zinssatz) /100));
			anzahlJahre ++;
		}
		System.out.printf("\nGl�ckwunsch! Nach %d Jahren sind Sie Million�r.", anzahlJahre);
	}

}
