import java.util.Scanner;

public class Aufgabe2_for {

	public static void main(String[] args) {
		System.out.print("Bitte geben Sie einen begrenzenden Wert ein: ");
		Scanner eingabe = new Scanner(System.in);
		int n = eingabe.nextInt();
		int summe = 0;
		
		for (int i = 1; i <= n; i++)
			summe += i;  
			
		System.out.println("Die Summe von a) betr�gt: " + summe);
		
		summe = 0;
		for (int i = 1; i <= n/2; i++)
			summe += 2*i;
		
		System.out.println("Die Summe von b) betr�gt: " + summe);
		
		summe = 1;
		for (int i = 1; i <= n/2-1; i++)
			summe += 2*i+1;
		
		System.out.println("Die Summe von c) betr�gt: " + summe);

	}

}
