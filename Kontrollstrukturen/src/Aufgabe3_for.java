
public class Aufgabe3_for {

	public static void main(String[] args) {
		
		System.out.println("Diese Zahlen von 1 bis 200 sind durch 7 teilbar:");
		for (int i = 1; i<=200; i++) 
		{
			if (i % 7 == 0) 
				System.out.print(i + " ");	
		}
		System.out.println("\n\nDiese Zahlen von 1 bis 200 sind nicht durch 5, aber durch 4 teilbar:");
		for (int i = 1; i<=200; i++) 
		{
			if (i % 5 != 0 && i % 4 == 0)
				System.out.print(i + " ");		
		}

	}

}
