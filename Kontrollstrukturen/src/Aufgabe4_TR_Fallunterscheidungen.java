import java.util.Scanner;

public class Aufgabe4_TR_Fallunterscheidungen {

	public static void main(String[] args) {
		double zahl1 = 0;
		double zahl2 = 0;
		double ergebnis = 0;
		char operation;
		Scanner Eingabe = new Scanner (System.in);
		
		System.out.println("Guten Tag, dies ist ein einfacher Rechner, der mit zwei Zahlen rechnen kann.");
		
		System.out.println("\nBitte gebe Sie die erste Zahl ein.");
		zahl1 = Eingabe.nextDouble();
			
		System.out.println("Bitte geben Sie jetzt die zweite Zahl ein.");
		zahl2 = Eingabe.nextDouble();
		
		System.out.println("\nJetzt k�nnen Sie noch die Rechenoperation w�hlen:");
		System.out.println("Tippen Sie \"+\" f�r die Addition, \"-\" f�r die Subtraktion, \"*\" f�r die Multiplikation oder \"/\" f�r die Division ein.");
		operation = Eingabe.next().charAt(0);
		
		switch (operation)
		{
			case '+':
				ergebnis = zahl1 + zahl2;
				break;
			case '-':
				ergebnis = zahl1 - zahl2;
				break;
			case '*':
				ergebnis = zahl1 * zahl2;
				break;
			case '/':
				ergebnis = zahl1 / zahl2;
				break;
			default:
				System.out.println("Bei Ihrer Eingabe handelt es sich nicht um zwei Zahlen.");
				break;
		}
		
		System.out.printf("\nDas Ergebnis der Rechnung lautet: \n%.2f %s %.2f = %.2f.", zahl1, operation, zahl2, ergebnis);
	}

}
