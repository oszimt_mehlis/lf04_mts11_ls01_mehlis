import java.util.Scanner;

public class Aufgabe2_Auswahlstrukturen {

	public static void main(String[] args) {

		Scanner Eingabe = new Scanner(System.in);
		double nettobetrag;
		double bruttobetrag;
		String gewaehlterSteuersatz;

		System.out.println("Guten Tag, dies ist ein Progamm zur Berechnung des Steuersatzes!");
		System.out.println("Bitte geben Sie Ihren Nettobetrag ein.");

		nettobetrag = Eingabe.nextDouble();

		System.out.println("\nSoll der Bruttobetrag mit dem erm��igten oder vollen Steuersatz berechnet werden?");
		System.out.println("Geben Sie \"j\" f�r den erm��igten Steuersatz und \"n\" f�r den vollen Steuersatz ein.");

		gewaehlterSteuersatz = Eingabe.next();

		if (gewaehlterSteuersatz.equals("j")) 
		{
			bruttobetrag = (nettobetrag / 1.07);
			System.out.printf("\nDer Bruttobetrag mit erm��igtem Steuersatz (7 Prozent) betr�gt %.2f�", bruttobetrag);
		} 
		else if (gewaehlterSteuersatz.equals("n")) 
		{
			bruttobetrag = nettobetrag / 1.16;
			System.out.printf("\nDer Bruttobetrag mit vollem Steuersatz (16 Prozent) betr�gt %.2f�", bruttobetrag);
		}

	}

}
