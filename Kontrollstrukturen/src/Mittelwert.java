import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte fuer x und y festlegen:
		// ===========================
		Scanner scan = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
		int anzahl = scan.nextInt();
		
		// (V) Verarbeitung
		double summe = 0;
		int zaehlvariable = 0;

		while (zaehlvariable < anzahl) {
			System.out.println("Geben Sie eine Zahl ein: ");
			double x = scan.nextDouble();
			summe += x;
			zaehlvariable++;
		}

		/*for (int i = 0; i < anzahl; i++) {

			System.out.println("Geben Sie eine Zahl ein: ");
			double x = scan.nextDouble();
			summe += x;
		}*/
		
		double mittelwert = summe / anzahl;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert betr�gt %.2f\n", mittelwert);
	}
}
