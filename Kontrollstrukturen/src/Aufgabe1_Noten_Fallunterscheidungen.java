import java.util.Scanner;

public class Aufgabe1_Noten_Fallunterscheidungen {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);
		int note;
		
		System.out.println("Bitte geben Sie eine Note zwischen 1 und 6 als Zahl ein.");
		note = Eingabe.nextInt();
		
		System.out.println("\nVerschriftlich bedeutet Ihre Note:");
		switch (note)
		{
			case 1:
				System.out.println("sehr gut");
				break;
			case 2: 
				System.out.println("gut");
				break;
			case 3: 
				System.out.println("befriedigend");
				break;
			case 4: 
				System.out.println("ausreichend");
				break;
			case 5: 
				System.out.println("mangelhaft");
				break;
			case 6: 
				System.out.println("ungenügend");
				break;
			default: 
				System.out.println("Bei Ihrer Eingabe handelt es sch nicht um eine Note.");
				break;
		}

	}

}
