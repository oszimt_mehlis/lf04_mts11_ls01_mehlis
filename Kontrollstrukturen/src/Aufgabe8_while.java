import java.util.Scanner;

public class Aufgabe8_while {
	//Aufgabe Matrix
	public static void main(String[] args) {
		int zahl;
		int quersumme = 0;
		boolean enthaeltZahl;
		Scanner eingabe = new Scanner (System.in);
		
		System.out.println("Dieses Programm zeigt Ihnen eine Multiplikationsmatrix f�r eine bestimmte Zahl an.");
		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		zahl = eingabe.nextInt();
		
		System.out.println("");
		
		if ((zahl >= 2) && (zahl <= 9))
		{
			for (int i = 0; i <= 90; i=i+10)
			{
				for (int j = i; j <= i+10-1; j++)
				{	
					enthaeltZahl = false;
					quersumme = 0;
					int zuPruefendeZahl = j;
					while (0 != zuPruefendeZahl) 
					{
						quersumme = quersumme + (zuPruefendeZahl % 10);
						zuPruefendeZahl = zuPruefendeZahl / 10;
						if ((j % 10 == zahl) || (zuPruefendeZahl == zahl))
						{
							enthaeltZahl = true;
						}
					}
					if (quersumme == zahl)
					{
						System.out.print("* ");	
					}
					else if ((j % zahl == 0) && (j != 0))
					{
						System.out.print("* ");	
					}
					else if (enthaeltZahl == true)
					{
						System.out.print("* ");
					}
					else
					{
						System.out.print(j+ " ");
					}
					
				}
				System.out.println("");
			}
		}
		else
		{
			System.out.println("Bitte �berpr�fen Sie Ihre Eingabe.");
		}
	}

}
