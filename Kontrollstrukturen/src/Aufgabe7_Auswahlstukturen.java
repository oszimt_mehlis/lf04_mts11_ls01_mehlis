import java.util.Scanner;

public class Aufgabe7_Auswahlstukturen {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double zahl3;
		Scanner Eingabe =  new Scanner(System.in);
		
		System.out.println("Guten Tag, dies ist ein Programm, welches Ihre eingebenen Zahlen aufsteigend sortiert.");
		
		System.out.println("\nBitte geben Sie jetzt die erste Zahl ein.");
		zahl1 = Eingabe.nextDouble();
		
		System.out.println("Als n�chstes geben Sie die zweite Zahl ein.");
		zahl2 = Eingabe.nextDouble();
		
		System.out.println("Zuletzt geben Sie bitte die dritte Zahl ein.");
		zahl3 = Eingabe.nextDouble();
		
		System.out.println("\nDie richtige Reihenfolge lautet:");
		
		if ((zahl1 < zahl2) && (zahl1 < zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 < zahl3) && (zahl2 < zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 < zahl1) && (zahl3 < zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 == zahl2) && (zahl1 < zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 == zahl3) && (zahl2 < zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 == zahl1) && (zahl3 < zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 > zahl2) && (zahl1 < zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 > zahl3) && (zahl2 < zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 > zahl1) && (zahl3 < zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 > zahl2) && (zahl1 == zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 > zahl3) && (zahl2 == zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 > zahl1) && (zahl3 == zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 < zahl2) && (zahl1 > zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 < zahl3) && (zahl2 > zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 < zahl1) && (zahl3 > zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 > zahl2) && (zahl1 > zahl3))
		{
			System.out.println(zahl1);
		}
		else if ((zahl2 > zahl3) && (zahl2 > zahl1))
		{
			System.out.println(zahl2);
		}
		else if ((zahl3 > zahl1) && (zahl3 > zahl2))
		{
			System.out.println(zahl3);
		}
		
		if ((zahl1 == zahl2) && (zahl1 == zahl3))
		{
			System.out.println(zahl1);
		}
	}
}
