import java.util.Scanner;

public class Aufgabe1_while {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Bis zu welcher Zahl soll von 1 an gez�hlt werden?");
		
		int n = eingabe.nextInt();
		int zaehler = 1;
		while (zaehler <= n) 
		{
			System.out.print(zaehler + " ");
			zaehler ++;
		}

	}

}
