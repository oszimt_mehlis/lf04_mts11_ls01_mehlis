import java.util.Scanner;

public class Aufgabe8_for {

	public static void main(String[] args) {
		String abstand = "";
		Scanner eingabe = new Scanner(System.in);
		System.out.print("Bitte geben Sie die Seitenlšnge des Quadrats ein: ");
		int laenge = eingabe.nextInt();
		

		for (int i = 1; i <= laenge * 2 - 3; i++) 
		{
			abstand = abstand + " ";
		}
		System.out.println("");
		for (int i = 0; i <= laenge; i++) 
		{
			if (i == laenge - 1) {
				System.out.println("");
			}
			if ((i == 0) || (i == laenge)) 
			{
				for (int j = 0; j <= laenge - 1; j++) 
				{
					System.out.printf("%s", "* ");
				}
			}
			if ((i >= 1) && (i <= laenge - 2)) 
			{
				System.out.printf("\n%s%s%s", "*", abstand, "*");
			}
		}

	}

}
