import java.util.Scanner;

public class Aufgabe3_Auswahlstrukturen {

	public static void main(String[] args) {
		int anzahlMaeuse;
		int lieferpauschale;
		double einzelpreis;
		double rechnungsbetrag;
		Scanner Eingabe = new Scanner(System.in);
		
		System.out.println("Guten Tag, bitte geben Sie die Anzahl der M�use ein, die Sie bestellen wollen.");
		anzahlMaeuse = Eingabe.nextInt();
		
		System.out.println("\nBitte geben Sie als n�chstes den Einzelpreis der M�use ein.");
		einzelpreis = Eingabe.nextDouble();
		
		if (anzahlMaeuse < 10) 
		{
			lieferpauschale = 10;
			
		}
		else
		{
			lieferpauschale = 0;
		}
		rechnungsbetrag = (double)lieferpauschale + (einzelpreis*anzahlMaeuse)/1.16;
		
		System.out.printf("\nDer Rechnungbetrag f�r %d M�use mit einem Einzelpreis von %.2f Euro betr�gt %.2f Euro.", 
				anzahlMaeuse, einzelpreis, rechnungsbetrag);
	}

}
