import java.util.Scanner;

public class Aufgabe2_while {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein, um davon die Fakult�t zu berechnen (Zahl <= 20).");
		int n = eingabe.nextInt();
		
		int fakultaet = 1;
		if (n <= 20) 
		{
			System.out.print("\n" + n + "! = ");
			while (n <= 20 && n != 0) 
			{
				fakultaet *= n;
				n--;
			}
			System.out.print(fakultaet);
		}
		else
		{
			System.out.print("\nDie eingebene Zahl ist zu gro�!");
		}
	}

}
