import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		double zahl1 = myScanner.nextDouble();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		double zahl2 = myScanner.nextDouble();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		// int ergebnis = zahl1 + zahl2

		System.out.print("\nDas Ergebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + (int)(zahl1 + zahl2));
		System.out.print("\nDas Ergebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + (int)(zahl1 - zahl2));
		System.out.print("\nDas Ergebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + (int)(zahl1 * zahl2));
		System.out.print("\nDas Ergebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + (double)(zahl1 / zahl2));
		myScanner.close();

	}

}
