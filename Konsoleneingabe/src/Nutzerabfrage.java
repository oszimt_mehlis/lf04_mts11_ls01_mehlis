import java.util.Scanner;

public class Nutzerabfrage {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Guten Tag. Bitte nehmen Sie sich kurz Zeit, um drei Fragen zu beantworten.\n");

		System.out.println("Wie lautet Ihr Vorname?");
		String vorname = myScanner.next();

		System.out.println("\nWie hei�en Sie mit Nachnamen?");
		String nachname = myScanner.next();

		System.out.println("\nZuletzt m�chte ich Sie bitten Ihr Alter einzugeben.");
		short alter = myScanner.nextShort();

		String s = "\nVielen Dank, dass Sie an dieser kurzen Umfrage teilgenommen haben.";
		String t = " " + vorname + ',';
		String o = " " + nachname;
		String p = "Alter: " + alter + " Jahre";
		System.out.println(s);
		System.out.println("Ihre eingegebene Daten lauten:");
		System.out.printf("%23s%s\n%20s", "Vorname, Nachname:", t + o, p);

		myScanner.close();

	}

}
